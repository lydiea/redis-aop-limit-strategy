package com.jacklin.snowy.limitstrategy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * 线程池配置
 *
 * @author: jacklin
 * @since: 2022/5/10 14:19
 */
@Configuration
public class ThreadPoolConfig {

    /**
     * 生成令牌线程池
     *
     * @return 线程池实例化对象
     */
    @Bean(name = "tokenPushThreadPoolScheduler")
    public ThreadPoolTaskScheduler tokenPushThreadConfig() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(1);
        scheduler.setThreadNamePrefix("生成令牌桶线程");
        return scheduler;
    }

    /**
     * 漏桶滴水线程池
     *
     * @return 线程池实例化对象
     */
    @Bean(name = "leakyBucketPopThreadPoolScheduler")
    public ThreadPoolTaskScheduler leakyBucketPopThreadConfig() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(1);
        scheduler.setThreadNamePrefix("漏桶滴水线程");
        return scheduler;
    }
}
