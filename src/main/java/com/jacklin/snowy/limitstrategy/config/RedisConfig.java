package com.jacklin.snowy.limitstrategy.config;

import com.alibaba.fastjson.support.spring.FastJsonRedisSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * Redis配置
 *
 * @author: jacklin
 * @since: 2022/5/10 14:18
 */
@Configuration
public class RedisConfig {

    /**
     * 初始化RedisTemplate，默认使用的是JDKSerializer的序列方式，效率低，这里配置使用FastJsonRedisSerializer
     *
     * @return redisTemplate
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        //key序列化
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        //value序列化
        redisTemplate.setValueSerializer(new FastJsonRedisSerializer<>(Object.class));
        //Hash Key序列化
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        //Hash Value序列表
        redisTemplate.setHashValueSerializer(new StringRedisSerializer());
        redisTemplate.setConnectionFactory(factory);
        return redisTemplate;
    }
}
