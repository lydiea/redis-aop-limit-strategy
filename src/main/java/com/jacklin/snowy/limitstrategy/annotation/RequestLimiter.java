package com.jacklin.snowy.limitstrategy.annotation;

import com.jacklin.snowy.limitstrategy.enums.RequestLimitType;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 自定义限流注解
 *
 * @author: jacklin
 * @since: 2022/5/10 12:05
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Order(Ordered.HIGHEST_PRECEDENCE)
public @interface RequestLimiter {

    /**
     * 限流类型 ，具体见枚举类 com.jacklin.snowy.limitstrategy.enums.RequestLimitType
     */
    RequestLimitType type() default RequestLimitType.TOKEN;

    /**
     * 限流访问数
     */
    int limitCount() default 100;

    /**
     * 限流时间段
     */
    long time() default 60;

    /**
     * 限流时间段 时间单位
     */
    TimeUnit unit() default TimeUnit.SECONDS;

    /**
     * 漏出或者生成令牌时间间隔，单位 毫秒  (当type为TOKEN、LEAKY_BUCKET时生效)
     */
    long period() default 1000;

    /**
     * 每次生成令牌数或者漏出水滴数  (当type为TOKEN、LEAKY_BUCKET时生效)
     */
    int limitPeriodCount() default 10;
}
