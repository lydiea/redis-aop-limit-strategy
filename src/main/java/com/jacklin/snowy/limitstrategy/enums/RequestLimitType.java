package com.jacklin.snowy.limitstrategy.enums;

/**
 * 限流模式
 *
 * @author: jacklin
 * @since: 2022/5/10 12:00
 */
public enum RequestLimitType {

    /**
     * 令牌算法
     */
    TOKEN(1, "令牌算法"),
    /**
     * 漏桶算法
     */
    LEAKY_BUCKET(2, "漏桶算法"),

    /**
     * 固定窗口
     */
    FIXED_WINDOW(3, "固定窗口"),
    /**
     * 滑动窗口
     */
    SLIDE_WINDOW(4, "滑动窗口");

    private Integer type;
    private String desc;

    RequestLimitType(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
