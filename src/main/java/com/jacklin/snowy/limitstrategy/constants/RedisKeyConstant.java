package com.jacklin.snowy.limitstrategy.constants;

/**
 * redis相关常量
 *
 * @author: jacklin
 * @since: 2022/5/10 14:22
 */
public interface RedisKeyConstant {

    interface RequestLimit {

        /**
         * 令牌桶键名
         */
        String QPS_TOKEN = "request:limit:qps:tokenBucket:";

        /**
         * 漏桶键名
         */
        String QPS_LEAKY_BUCKET = "request:limit:qps:leakyBucket:";

        /**
         * 固定窗口键名
         */
        String QPS_FIXED_WINDOW = "request:limit:qps:fixedWindow:";

        /**
         * 滑动窗口键名
         */
        String QPS_SLIDE_WINDOW = "request:limit:qps:slideWindow:";
    }

}
