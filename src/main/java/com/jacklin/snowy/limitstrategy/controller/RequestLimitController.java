package com.jacklin.snowy.limitstrategy.controller;

import com.jacklin.snowy.limitstrategy.annotation.RequestLimiter;
import com.jacklin.snowy.limitstrategy.enums.RequestLimitType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试限流控制器
 *
 * @author: jacklin
 * @since: 2022/5/10 14:20
 */
@RestController
@RequestMapping()
public class RequestLimitController {

    /**
     * 测试AOP拦截 令牌桶 限流
     *
     * @return 返回结果
     */
    @RequestLimiter(type = RequestLimitType.TOKEN, limitCount = 2, limitPeriodCount = 1, period = 3000)
    @GetMapping("/token-bucket-test")
    public String tokenBucketTest() {
        return "【" + RequestLimitType.TOKEN.getDesc() + "】限流 - 接口返回";
    }

    /**
     * 测试AOP拦截 漏桶 限流
     *
     * @return 返回结果
     */
    @RequestLimiter(type = RequestLimitType.LEAKY_BUCKET, limitCount = 10, limitPeriodCount = 1, period = 3000)
    @GetMapping("/leaky-bucket-test")
    public String leakyBucketTest() {
        return "【" + RequestLimitType.LEAKY_BUCKET.getDesc() + "】限流 - 接口返回";
    }


    /**
     * 测试AOP拦截 固定窗口限流
     *
     * @return 返回结果
     */
    @RequestLimiter(type = RequestLimitType.FIXED_WINDOW, limitCount = 2, time = 5)
    @GetMapping("/fixed-window-test")
    public String fixedWindowTest() {
        return "固定窗口限流 - 接口返回";
    }

    /**
     * 测试AOP拦截 滑动窗口 限流
     *
     * @return 返回结果
     */
    @RequestLimiter(type = RequestLimitType.SLIDE_WINDOW, limitCount = 2, time = 5)
    @GetMapping("/slide-window-test")
    public String slideWindowTest() {
        return "滑动窗口限流 - 接口返回";
    }

}
