package com.jacklin.snowy.limitstrategy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisAopLimitStrategyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisAopLimitStrategyApplication.class, args);
    }

}
