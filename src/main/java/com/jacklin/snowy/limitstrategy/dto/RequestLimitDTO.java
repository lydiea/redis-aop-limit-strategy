package com.jacklin.snowy.limitstrategy.dto;

import com.jacklin.snowy.limitstrategy.annotation.RequestLimiter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 限流参数DTO
 *
 * @author: jacklin
 * @since: 2022/5/10 12:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestLimitDTO implements Serializable {

    /**
     * 限流配置
     */
    private RequestLimiter limiter;

    /**
     * 拦截建
     */
    private String key;
}
