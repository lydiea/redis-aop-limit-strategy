package com.jacklin.snowy.limitstrategy.service.impl;

import com.jacklin.snowy.limitstrategy.annotation.RequestLimiter;
import com.jacklin.snowy.limitstrategy.constants.RedisKeyConstant;
import com.jacklin.snowy.limitstrategy.dto.RequestLimitDTO;
import com.jacklin.snowy.limitstrategy.enums.RequestLimitType;
import com.jacklin.snowy.limitstrategy.service.RequestLimitService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 令牌桶算法 限流
 *
 * @author: jacklin
 * @since: 2022/5/10 14:11
 */
@Slf4j
@Service
public class TokenBucketRateLimitServiceImpl implements RequestLimitService {

    @javax.annotation.Resource(name = "tokenPushThreadPoolScheduler")
    private ThreadPoolTaskScheduler scheduler;


    @Value("${request-limit.scan-package:}")
    private String scanPackage;

    @Autowired
    private ResourcePatternResolver resourcePatternResolver;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public boolean checkRequestLimit(RequestLimitDTO dto) {
        Object pop = redisTemplate.opsForList().rightPop(RedisKeyConstant.RequestLimit.QPS_TOKEN + dto.getKey());
        RequestLimiter limiter = dto.getLimiter();
        log.info("限流配置：每 {} 毫秒 生成 {} 个令牌，最大令牌数：{}", limiter.period(), limiter.limitPeriodCount(), limiter.limitCount());
        if (pop == null) {
            log.info("【{}】限流控制，令牌桶中不存在令牌，请求被拦截", dto.getKey());
            return true;
        } else {
            log.info("【{}】令牌桶存在令牌，未达到限流值，放行", dto.getKey());
            return false;
        }
    }

    @Override
    public RequestLimitType getType() {
        return RequestLimitType.TOKEN;
    }

    /**
     * 定速生成令牌
     */
    @PostConstruct
    public void pushToken() {
        // 扫描出所有使用了自定义注解并且限流类型为令牌算法的方法信息
        List<RequestLimitDTO> list = this.getTokenLimitList(resourcePatternResolver, RequestLimitType.TOKEN, scanPackage);
        if (list.isEmpty()) {
            log.info("未扫描到使用 令牌限流 注解的方法，结束生成令牌线程");
            return;
        }

        // 每个接口方法更具注解配置信息提交定时任务，生成令牌进令牌桶
        list.forEach(limit -> scheduler.scheduleAtFixedRate(() -> {

            String key = RedisKeyConstant.RequestLimit.QPS_TOKEN + limit.getKey();
            Long tokenSize = redisTemplate.opsForList().size(key);

            int size = tokenSize == null ? 0 : tokenSize.intValue();
            if (size >= limit.getLimiter().limitCount()) {
                log.info("【{}】令牌数量已达最大值【{}】，丢弃新生成令牌", key, size);
                return;
            }
            // 判断添加令牌数量
            int addSize = Math.min(limit.getLimiter().limitPeriodCount(), limit.getLimiter().limitCount() - size);
            List<String> addList = new ArrayList<>(addSize);
            for (int index = 0; index < addSize; index++) {
                addList.add(UUID.randomUUID().toString());
            }
            redisTemplate.opsForList().leftPushAll(key, addList);
            log.info("【{}】生成令牌丢入令牌桶，当前令牌数：{}，令牌桶容量：{}", key, size + addSize, limit.getLimiter().limitCount());
        }, limit.getLimiter().period()));
    }
}
